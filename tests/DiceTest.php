<?php

namespace Test;

use App\Controller\DiceController;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Constraint\IsType;

class DiceControllerTest extends TestCase
{
    public function testRoll($max = 20)
    {
        $result = new DiceController();
        $results = $result->roll($max);
        $this->assertInternalType(IsType::TYPE_INT, $results);
        $this->assertLessThanOrEqual($max, $results);
    }
}