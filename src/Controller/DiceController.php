<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Exceptions\IncorrectDiceException;
use App\Exceptions\IncorrectMultiplierException;
use App\Exceptions\IncorrectCorrectorException;

class DiceController extends AbstractController
{
    public $diceType =[2, 3, 4, 5, 6, 8, 10, 12, 15, 20, 30, 50, 100];
    /**
     * @param int $dice 
     * @param int $multiplier
     * @param int $corrector
     * @Route("/dice/{dice}/{multiplier}/{corrector}", name="dice")
     */
    public function roll(int $dice, int $multiplier, int $corrector): Response //Dice throwing function
    {
        if (!in_array($dice,$this->diceType)) {
            throw new IncorrectDiceException(); //This condition checks if the user is typing a correct dice value according to those in the array ($diceType)
        }
        if ($multiplier < 0 || $multiplier > 100) {
            throw new IncorrectMultiplierException(); // This condition checks if the user is typing a correct multiplier value (here $multiplier should be between 1 and 99)
        }
        if ($corrector < -1000 || $corrector > 1000) { // This condition checks if the user is typing a correct corrector value (here $corrector should be between -999 and +999)
            throw new IncorrectCorrectorException();
        }
        $resultat = 0; 
        for ($i=0; $i <= $multiplier; $i++) {
            $resultat += rand(1, $dice);
        }
        $resultat += $corrector; //add the corrector to the resultat
        return new Response(
            $resultat, 
            Response::HTTP_OK
        );
    }
}