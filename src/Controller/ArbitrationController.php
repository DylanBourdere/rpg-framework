<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ArbitrationController extends AbstractController
{
    /**
     * @Route("/arbitration", name="arbitration")
     */
    public function index()
    {
        return $this->render('arbitration/index.html.twig', [
            'controller_name' => 'ArbitrationController',
        ]);
    }
}